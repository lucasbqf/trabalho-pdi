package formas;

import java.util.Vector;

public class Reta extends FormaGeometrica{
	public Reta(int x1, int y1, int x2,int y2){
		Pontos = new Vector<>();
		Pontos.add(new Ponto2D(x1, y1));
		Pontos.add(new Ponto2D(x2, y2));
	}
}
