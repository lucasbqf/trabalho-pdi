package formas;

import java.util.List;
import java.util.Vector;

public abstract class FormaGeometrica {
	public Vector<Ponto2D> Pontos;
	Ponto2D centro;
	
	public List<Ponto2D> getPontos(){
	return Pontos;
	}
	void setPontos(Vector<Ponto2D> pontos){
		this.Pontos = pontos;
	}
	void addPonto (int x,int y){
		Pontos.add(new Ponto2D(x, y));
	}
	
	public void translacao(int x, int y){
		for(Ponto2D ponto : Pontos){
			ponto.transladarX(x);
			ponto.transladarY(y);
		}
	}
}
