package formas;

import java.util.Vector;

public class Quadrilatero extends FormaGeometrica{
	public Quadrilatero(int x1, int y1,int d1, int d2){
		Pontos = new Vector<>();
		Pontos.add(new Ponto2D(x1, y1));
		Pontos.add(new Ponto2D(x1+d1, y1));
		Pontos.add(new Ponto2D(x1+d1, y1+d2));
		Pontos.add(new Ponto2D(x1, y1+d2));
		Pontos.add(new Ponto2D(x1, y1));
		centro= new Ponto2D(x1+(d1/2) , y1+(d2/2));
	}
	public Quadrilatero(int x, int y, int d){
		Pontos = new Vector<>();
		Pontos.add(new Ponto2D(x, y));
		Pontos.add(new Ponto2D(x + d, y));
		Pontos.add(new Ponto2D(x + d, y+d));
		Pontos.add(new Ponto2D(x, y+d));
		Pontos.add(new Ponto2D(x, y));
		centro= new Ponto2D(x+(d/2) , y+(d/2));
	}
}
