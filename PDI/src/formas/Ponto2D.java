package formas;

public class Ponto2D {
	private int[] coords = {0,0};
	public Ponto2D(int x, int y)
	{
		this.setX(x);
		this.setY(y);
	}
	public void setX(int x)
	{
		this.coords[0] = x;
	}
	public int getX()
	{
		return coords[0];
	}
	public void setY(int y)
	{
		this.coords[1] = y;
	}
	public int getY()
	{
		return coords[1];
	}
	public void transladarX(int x){
		this.coords[0] =coords[0]+x; 
	}
	public void transladarY(int y){
		this.coords[1] =coords[1]+y; 
	}
	public void setCoords(int x,int y,int norm){
		this.coords[0]=x;
		this.coords[1]=y;
		this.coords[2]=norm;
	}
	public int[] getCoords(){
		return this.coords;
	}
	
}
