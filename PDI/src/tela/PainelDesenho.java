package tela;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;



import java.awt.Graphics;


import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import listeners.AtualizaPosicaoListener;

@SuppressWarnings("serial")
class PainelDesenho extends JPanel implements AtualizaPosicaoListener{
	int x,y;
	JLabel LabelPosicaoMouse;
	PDesenho areaDesenho;
	JScrollPane SAreaDesenho;
	public PainelDesenho(){
		areaDesenho = new PDesenho();
		SAreaDesenho = new JScrollPane(areaDesenho);
		LabelPosicaoMouse = new JLabel("x: |y:");
		areaDesenho.addListener(this);
		
		areaDesenho.setPreferredSize(new Dimension(1000,1000));
		SAreaDesenho.setPreferredSize(new Dimension(300,300));
		areaDesenho.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		super.setBackground(Color.gray);
		
		setLayout(new BorderLayout());
		//add(SAreaDesenho, BorderLayout.CENTER);
		add(areaDesenho, BorderLayout.CENTER);
		add(LabelPosicaoMouse,BorderLayout.SOUTH);
		
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		areaDesenho.repaint();
		LabelPosicaoMouse.setText("x:"+ areaDesenho.x + "|y:" + areaDesenho.y);
	}

	@Override
	public void AtualizouPosicao() {
		this.x = areaDesenho.x;
		this.y = areaDesenho.y;
		repaint();
		
	}
}
