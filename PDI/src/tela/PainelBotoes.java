package tela;


import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class PainelBotoes extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	PainelFormas PFormas;
	PainelTransformacoes PTransformacoes;
	PainelIntens PIntens;
	JLabel labelFormas;
	JLabel labelTransformacoes;
	JLabel labelIntens;
	JLabel labelTipoEntrada;
	JRadioButton RBentradaDialogo;
	JRadioButton RBentradaMouse;
	ButtonGroup RBMenu;
	
	public PainelBotoes() {
		labelTipoEntrada = new JLabel("TIPO DE ENTRADA:");
		RBentradaDialogo = new JRadioButton("por dialogos");
		RBentradaMouse = new JRadioButton("por Mouse");
		RBMenu = new ButtonGroup();
		RBMenu.add(RBentradaDialogo);
		RBMenu.add(RBentradaMouse);
		PFormas = new PainelFormas();
		PIntens = new PainelIntens();
		PTransformacoes = new PainelTransformacoes();
		labelFormas = new JLabel("FORMAS:");
		labelTransformacoes = new JLabel("TRANSFORMACOES:");
		labelIntens = new JLabel("LISTA DE ITENS:");
		//setLayout(new GridLayout(3, 1));
		setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		add(labelTipoEntrada);
		add(RBentradaDialogo);
		add(RBentradaMouse);
		add(labelFormas);
		add(PFormas);
		add(labelTransformacoes);
		add(PTransformacoes);
		add(labelIntens);
		add(PIntens);
	}
}
