package tela;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;

import formas.Ponto;
import formas.Quadrilatero;
import formas.Reta;


@SuppressWarnings("serial")
public class TelaIsometrica extends JFrame{
	@SuppressWarnings("static-access")
	public TelaIsometrica(){
		super("Modo de mapeamento isotrópico.");
		//this.setPreferredSize(new Dimension(500,500));
		addWindowListener(new WindowAdapter()
			{
				public void windowClosing(WindowEvent e)
				{
					System.exit(0);
				}
			}
		);
		PainelDesenho PDesenho =new PainelDesenho();
		PainelBotoes PBotoes = new PainelBotoes(); 
		setLayout(new BorderLayout());
		//setLayout(new GridLayout(0,2));
		add(PBotoes,BorderLayout.WEST);
		add(PDesenho,BorderLayout.CENTER);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
        setLocationRelativeTo(null);
        
        JOptionPane escolhaPonto= new JOptionPane();
        PBotoes.PFormas.BPonto.addActionListener(new ActionListener() {
		
			@Override
			public void actionPerformed(ActionEvent e) {
				if(PBotoes.RBentradaDialogo.isSelected() == true){
					System.out.println("apertou botao ponto");
					String x = escolhaPonto.showInputDialog(rootPane, "escolha o valor de x");
					String y = escolhaPonto.showInputDialog(rootPane, "escolha o valor de y");
					PDesenho.areaDesenho.listaDesenhos.add(new Ponto(Integer.parseInt(x),Integer.parseInt(y)));
					PDesenho.areaDesenho.repaint();
				}
				
				PDesenho.areaDesenho.repaint();
			}
		});;
		PBotoes.PFormas.BReta.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(PBotoes.RBentradaDialogo.isSelected() == true){
					String x1 = escolhaPonto.showInputDialog(rootPane, "escolha o valor de x1");
					String y1 = escolhaPonto.showInputDialog(rootPane, "escolha o valor de y1");
					String x2 = escolhaPonto.showInputDialog(rootPane, "escolha o valor de x2");
					String y2 = escolhaPonto.showInputDialog(rootPane, "escolha o valor de y2");
					PDesenho.areaDesenho.listaDesenhos.add(new Reta(Integer.parseInt(x1), Integer.parseInt(y1), Integer.parseInt(x2), Integer.parseInt(y2)));
				}
				PDesenho.areaDesenho.repaint();
			}
		});
        PBotoes.PFormas.BQuadrado.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(PBotoes.RBentradaDialogo.isSelected() == true){
					String x1 = escolhaPonto.showInputDialog(rootPane, "escolha o valor de x1");
					String y1 = escolhaPonto.showInputDialog(rootPane, "escolha o valor de y1");
					String d = escolhaPonto.showInputDialog(rootPane, "escolha o valor da distancia");
					PDesenho.areaDesenho.listaDesenhos.add(new Quadrilatero(Integer.parseInt(x1), Integer.parseInt(y1), Integer.parseInt(d)));
				
				}
				PDesenho.areaDesenho.repaint();
			}
		
		});
        PBotoes.PFormas.BRetangulo.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(PBotoes.RBentradaDialogo.isSelected() == true){
					String x1 = escolhaPonto.showInputDialog(rootPane, "escolha o valor de x1");
					String y1 = escolhaPonto.showInputDialog(rootPane, "escolha o valor de y1");
					String d1 = escolhaPonto.showInputDialog(rootPane, "escolha o valor da distancia horizontal");
					String d2 = escolhaPonto.showInputDialog(rootPane, "escolha o valor da distancia vertical");
					PDesenho.areaDesenho.listaDesenhos.add(new Quadrilatero(Integer.parseInt(x1), Integer.parseInt(y1), Integer.parseInt(d1), Integer.parseInt(d2)));
				}
				PDesenho.areaDesenho.repaint();
				
			}
		});
	}	
	
}
