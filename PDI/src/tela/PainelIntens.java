package tela;

import java.awt.BorderLayout;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class PainelIntens extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JList<String> listaItens;
	DefaultListModel<String> listaPadrao;
	JScrollPane SListaItens;
	JButton Bdeleta;
	public PainelIntens() {
		listaPadrao = new DefaultListModel<>();
		Bdeleta = new JButton("APAGAR");
		listaItens = new JList<>(listaPadrao);
		SListaItens = new JScrollPane(listaItens);
		setLayout(new BorderLayout());
		add(SListaItens,BorderLayout.CENTER);
		add(Bdeleta, BorderLayout.SOUTH);
		
	}
	
	
}
