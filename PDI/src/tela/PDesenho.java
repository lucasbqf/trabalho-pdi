package tela;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JPanel;

import formas.FormaGeometrica;
import formas.Ponto2D;
import formas.Quadrilatero;
import formas.Reta;
import listeners.AtualizaPosicaoListener;

public class PDesenho extends JPanel {
	private static final long serialVersionUID = 1L;
	int x=20, y=20;
	List<FormaGeometrica> listaDesenhos;
	private List <AtualizaPosicaoListener> ouvintesPosicao;
	public void addListener(AtualizaPosicaoListener novoOuvinte){
		ouvintesPosicao.add(novoOuvinte);
	}
	public void atualizaPosicao(){
		for(AtualizaPosicaoListener ouvinte : ouvintesPosicao){
			ouvinte.AtualizouPosicao();
		}
	}
	
	
	public PDesenho() {
		ouvintesPosicao = new ArrayList<>();
		listaDesenhos = new Vector<>();
		listaDesenhos.add(new Reta(100,100,100,200));
		listaDesenhos.add(new Reta(200,50,400,70));
		super.setBackground(Color.white);
		class OuvinteMouse implements MouseListener,MouseMotionListener{

			@Override
			public void mouseDragged(MouseEvent e) {
			
				listaDesenhos.add(new Reta(x, y,e.getX(),e.getY()));
				System.out.println("arrastou para :"+ x +"," + y);
				atualizaPosicao();
				//repaint();
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				x = e.getX();
				y = e.getY();
				System.out.println("moveu para :"+ x +"," + y);
				atualizaPosicao();
				
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				x = e.getX();
				y = e.getY();
				listaDesenhos.add(new Quadrilatero(x, y, 10));
				System.out.println("clicou em :"+ x +"," + y);
				atualizaPosicao();
				repaint();
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				System.out.println("mouse entrou!");
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				System.out.println("mouse saiu!");
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				System.out.println("segurou em:"+e.getX() +":"+e.getY());
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				System.out.println("Soutou em:"+e.getX() +":"+e.getY());
				
			}
			
		}
		OuvinteMouse ma = new OuvinteMouse();
		addMouseListener(ma);
		addMouseMotionListener(ma);
	}
	
	static void desenhaPonto(Graphics g, int x,int y){
		g.drawLine(x, y, x, y);
		
	}
	public static void desenhalinha(Graphics g, int xP, int yP, int xQ, int yQ)
	{
		int x = xP;
		int y = yP;
		int d = 0;
		int dx = xQ - xP;
		int dy = yQ - yP;
		int xInc = 1;
		int yInc = 1;
		int c;
		int m;
		
		if(dx < 0)
		{ 
			xInc = -1;
			dx = -dx;
		}
		if(dy < 0)
		{
			yInc = -1;
			dy = -dy;
		}
		if(dy <= dx)
		{
			c = 2 * dx;
			m = 2 * dy;
			if(xInc < 0) 
				dx++;
			for(;;)
			{
				desenhaPonto(g, x, y);
				if(x == xQ)
					break;
				x += xInc;
				d += m;
				if(d >= dx)
				{
					y += yInc;
					d -= c;
				}				
			}
		}
		else
		{
			c = 2 * dy;
			m = 2 * dx;
			if(yInc < 0) 
				dy++;
			for(;;)
			{
				desenhaPonto(g, x, y);
				if(y == yQ)
					break;
				y += yInc;
				d += m;
				if(d >= dy)
				{
					x += xInc;
					d -= c;
				}
			}
		}
	}
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int k =0;
		for(FormaGeometrica forma : listaDesenhos){
			System.out.println("passou" + k);
			Ponto2D de = null;
			for(Ponto2D ponto : forma.Pontos){
				if(de != null){
					desenhalinha(g,de.getX(),de.getY(), ponto.getX(),ponto.getY());
					System.out.println("de:"+ de.getX() +":"+ de.getY());
					System.out.println("ponto:"+ ponto.getX() +":"+ ponto.getY());
				}
				de= ponto;
			}
			k++;
		}
	}

}

