package tela;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

public class PainelTransformacoes extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JButton BRotacao;
	JButton BTranslacao;
	JButton BEscala;
	JButton BCisalhamento;
	JButton BReflexao;
	
	PainelTransformacoes(){
		setLayout(new GridLayout(0,2));
		BRotacao = new JButton("ROTACAO");
		BTranslacao = new JButton("TRANSLACAO");
		BEscala = new JButton("ESCALA");
		BCisalhamento = new  JButton("CISALHAMENTO");
		BReflexao = new JButton("REFLEXAO");
		
		add(BRotacao);
		add(BTranslacao);
		add(BEscala);
		add(BCisalhamento);
		add(BReflexao);
	}
	
}
