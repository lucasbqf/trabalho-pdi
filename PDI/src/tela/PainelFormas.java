package tela;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class PainelFormas extends JPanel {
	JButton BReta;
	JButton BPonto;
	JButton BTriangulo;
	JButton BQuadrado;
	JButton BRetangulo;
	JButton BBezier;
	
	public PainelFormas() {
		this.setLayout(new GridLayout(3,2));
		BReta = new JButton("RETA");
		BPonto = new JButton("PONTO");
		BTriangulo = new JButton("TRIANGULO");
		BQuadrado = new JButton("QUADRADO");
		BRetangulo = new JButton("RETANGULO");
		BBezier = new JButton("BEZIER");
		
		
		add(BReta);
		add(BPonto);
		add(BTriangulo);
		add(BQuadrado);
		add(BRetangulo);
		add(BBezier);
	}
	
	
	
	
}
